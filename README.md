# Bilingual Word Embeddings for Lower Resource Languages (LRLs) using VecMap

## Implementation Design
![Implementation Design](images/implementation_design.png)

## Requirements
* Python 3
* VecMap
* Fasttext / Word2Vec
* ...

## Acknowledgement
Thanks are due to our professors, Gorka Labaka Intxauspe and Nora Aranberri Monasterio, for their support and guidance throughout this project. Thank you to our group members Mariana and Zabrina who worked together to do this project. We also would like to thank Mikel Artetxe et al. that developed [VecMap](https://github.com/artetxem/vecmap). Furthermore, we thank to [OPUS](https://opus.nlpl.eu/) and [Axolotl Parallel Corpus](https://axolotl-corpus.mx/) that provides the corpora we used in this research.